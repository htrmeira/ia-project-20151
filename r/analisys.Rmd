---
title: "Comparing Learning Algorithms applied to Face Detection with Weka"
author: "Heitor Meira and Antonio Rodrigues"
date: "28-07-2015"
output: html_document
---
Results here were generated using the code in https://github.com/htrmeira/ia-project-20151.

The database from http://cbcl.mit.edu/projects/cbcl/software-datasets/FaceData1Readme.html (thanks Center for Biological and Computational Learning at MIT. =D)

It contains:

Training set: 2,429 faces, 4,548 non-faces 

Test set: 472 faces, 23,573 non-faces

***The environment:***

**Hardware:**

* 8 x Intel(R) Xeon(R) CPU E5620  @ 2.40GHz

* 12GB RAM

* 1TB HD

**Software:**

* Weka 3.6.12

* Java(TM) SE Runtime Environment (build 1.8.0_51-b16)

* Ubuntu 14.04.2 LTS 

* Kernel 3.13.0-49-generic x86_64

Our code basically get the image, extracts the gray scale as a histogram and executes the classification methos on this.

```{r, echo=FALSE}
mlp_data <- read.csv("/tmp/MultilayerPerceptron.csv", header=TRUE, sep=",")

library("ggplot2")
library("plyr")
```
***Classifier: Multilayer Perceptron***

Parameters tested:

- training_times: [250, 500, 750]

- learning_rates: [0.1, 0.3, 0.5]

- hidden_layers: [1, 3, 5, 10]

This is a simple group bar graph relating Precision[Recall and Duration] (y-axis), Training Time (x-axis), Learning Rate (first parameter each box) and number of Hidden Layers (second parameter of each box).

```{r, echo=FALSE}
mlp_parameters <- ddply(mlp_data, .(training_time, learning_rate, hidden_layer), summarize, precision=mean(precision), recall=mean(recall), duration=mean(duration))

ggplot(data=mlp_parameters, aes(x=training_time, y=precision, fill=as.factor(training_time))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0,1)) +
  facet_wrap(~learning_rate+hidden_layer, nrow=3) +
  xlab("Training Time") +
  ylab("Precision") +
  ggtitle("MultilayerPerceptron - Precision") +
  guides(fill=guide_legend("Training Time"))

ggplot(data=mlp_parameters, aes(x=training_time, y=recall, fill=as.factor(training_time))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0,1)) +
  facet_wrap(~learning_rate+hidden_layer, nrow=3) +
  xlab("Training Time") +
  ylab("Recall") +
  ggtitle("MultilayerPerceptron - Recall") +
  guides(fill=guide_legend("Training Time"))

ggplot(data=mlp_parameters, aes(x=training_time, y=duration/1000, fill=as.factor(training_time))) +
  geom_bar(position="dodge", stat="identity") +
  facet_wrap(~learning_rate+hidden_layer, nrow=3) +
  xlab("Training Time") +
  ylab("Duration (Seconds)") +
  ggtitle("MultilayerPerceptron - Duration") +
  guides(fill=guide_legend("Training Time"))
```

Now, discarding lower values and zooming for the top of the bars, we have more information.

```{r, echo=FALSE}
ggplot(data=mlp_parameters, aes(x=training_time, y=precision, fill=as.factor(training_time))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.96,0.966)) +
  facet_wrap(~learning_rate+hidden_layer, nrow=3) +
  xlab("Training Time") +
  ylab("Precision") +
  ggtitle("MultilayerPerceptron - Precision\n(Zoom)") +
  guides(fill=guide_legend("Training Time"))

ggplot(data=mlp_parameters, aes(x=training_time, y=recall, fill=as.factor(training_time))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.77,0.94)) +
  facet_wrap(~learning_rate+hidden_layer, nrow=3) +
  xlab("Training Time") +
  ylab("Recall") +
  ggtitle("MultilayerPerceptron - Recall\n(Zoom)") +
  guides(fill=guide_legend("Training Time"))
```

```{r, echo=FALSE}
mlp_orderes_by_precision <- arrange(mlp_parameters, desc(precision))
mlp_highest_precision <- mlp_orderes_by_precision[0:1,]
mlp_highest_precision

mlp_orderes_by_recall <- arrange(mlp_parameters, desc(recall))
mlp_highest_recall <- mlp_orderes_by_recall[0:1,]
mlp_highest_recall

mlp_precision_and_recall <- rbind(mlp_highest_precision, mlp_highest_recall)
mlp_precision_and_recall
```
- With training_time 750, learning_rate 0.1 and 10 hidden_layers, we have the best multilayer precision (96.54%). On the other side, the recall in this configuration is only 83.25%. A curious point to note is the duration of execution that is around 398.8s.

- With training_time 250, learning_rate of 0.3 and 3 hidden_layers, this technique presents the best recall, around 92.35% with 96.28% of precision, also worthing note is the duration that is about 4x faster than the one used by the first configuration.

***Classifier: Random Forest***

Parameters tested:

- max_depths: [0, 25, 50, 100, 200]

- num_features: [0, 25, 50, 100, 200]

- num_trees: [50, 100, 150]

This is group bar graph relating Precision[Recall and Duration] (y-axis), Max Depth (x-axis), Number of Trees (first parameter each box) and Number of Features (second paramter of each box).

```{r, echo=FALSE}
rf_data <- read.csv("/tmp/RandomForest.csv", header=TRUE, sep=",")
rf_parameters <- ddply(rf_data, .(max_depth, num_feature, num_tree), summarize, precision=mean(precision), recall=mean(recall), duration=mean(duration))

ggplot(data=rf_parameters, aes(x=max_depth, y=precision, fill=as.factor(max_depth))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.964,0.9653)) +
  facet_wrap(~num_tree+num_feature, nrow=3, ncol=5) +
  xlab("Max Depth") +
  ylab("Precision") +
  ggtitle("RandomForest - Precision\n(Zoom)") +
  guides(fill=guide_legend("Max Depth"))

ggplot(data=rf_parameters, aes(x=max_depth, y=recall, fill=as.factor(max_depth))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.931,0.945)) +
  facet_wrap(~num_tree+num_feature, nrow=3, ncol=5) +
  xlab("Max Depth") +
  ylab("Recall") +
  ggtitle("RandomForest - Recall\n(Zoom)") +
  guides(fill=guide_legend("Max Depth"))

ggplot(data=rf_parameters, aes(x=max_depth, y=duration/1000, fill=as.factor(max_depth))) +
  geom_bar(position="dodge", stat="identity") +
  facet_wrap(~num_tree+num_feature, nrow=3, ncol=5) +
  xlab("Max Depth") +
  ylab("Duration (Seconds)") +
  ggtitle("RandomForest - Duration") +
  guides(fill=guide_legend("Max Depth"))

rfp_ordered_by_precision <- arrange(rf_parameters, desc(precision))
rf_highest_precision <- rfp_ordered_by_precision[0:1,]
rf_highest_precision

rfp_ordered_by_recall <- arrange(rf_parameters, desc(recall))
rf_highest_recall <- rfp_ordered_by_recall[0:1,]
rf_highest_recall

rf_precision_and_recall <- rbind(rf_highest_precision, rf_highest_recall)
rf_precision_and_recall
```
- Good rates of recall and precision with max_depth of 0 (default weka - auto adjusting), num_features of 100 and num_trees of 50. Given us a precision of 96.51% and recall of 93.29%.

- With max_depth of 25, num_features of 25 and 150 num_trees, we could get a precision of 96.44% and recall of 94.39%.

- This two configurations presents similar duration time and good results, so they are both good configuration instances. However, we should pick the second one, for the slightly better recall.

***Classifier: Simple Logistic***

Parameters tested:

- heuristics_stop: [25, 50, 75]

- max_boosting_iterations: [250, 500, 750]

- cross_validations: [true, false]

This is group bar graph relating Precision[Recall and Duration] (y-axis), Use of Cross Validation (x-axis), Heuristic Stop (first parameter each box) and Max Boosting Iteration (second paramter of each box).
```{r, echo=FALSE}
sl_data <- read.csv("/tmp/SimpleLogistic.csv", header=TRUE, sep=",")
sl_parameters <- ddply(sl_data, .(heuristic_stop,max_boosting_iteration,use_cross_validation), summarize, precision=mean(precision), recall=mean(recall), duration=mean(duration))

ggplot(data=sl_parameters, aes(x=use_cross_validation, y=precision, fill=as.factor(use_cross_validation))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.957,0.962)) +
  facet_wrap(~heuristic_stop+max_boosting_iteration, nrow=3) +
  xlab("Using Cross Validation") +
  ylab("Precision") +
  ggtitle("SimpleLogistic - Precision\n(Zoom)") +
  guides(fill=guide_legend("Using Cross Validation"))

ggplot(data=sl_parameters, aes(x=use_cross_validation, y=recall, fill=as.factor(use_cross_validation))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.75,0.95)) +
  facet_wrap(~heuristic_stop+max_boosting_iteration, nrow=3) +
  xlab("Using Cross Validation") +
  ylab("Recall") +
  ggtitle("SimpleLogistic - Recall\n(Zoom)") +
  guides(fill=guide_legend("Using Cross Validation"))

ggplot(data=sl_parameters, aes(x=use_cross_validation, y=duration/1000, fill=as.factor(use_cross_validation))) +
  geom_bar(position="dodge", stat="identity") +
  facet_wrap(~heuristic_stop+max_boosting_iteration, nrow=3) +
  xlab("Using Cross Validation") +
  ylab("Duration (Seconds)") +
  ggtitle("SimpleLogistic - Duration") +
  guides(fill=guide_legend("Using Cross Validation"))

sl_ordered_by_precision <- arrange(sl_parameters, desc(precision))
sl_highest_precision <- sl_ordered_by_precision[0:1,]
sl_highest_precision

sl_ordered_by_recall <- arrange(sl_parameters, desc(recall))
sl_highest_recall <- sl_ordered_by_recall[0:1,]
sl_highest_recall

sl_precision_and_recall <- rbind(sl_highest_precision, sl_highest_recall)
sl_precision_and_recall
```

- Here we have the same precision (96.10%) and recall (93.5%) with the same configuration. With exception with use of cross validation (true), that provides better results, changing the configuration has little effect.

***Comparing the best results between classifier methods***

Simple bar graph comparing the best Precision and Recall between the used classifiers, we have:

```{r, echo=FALSE}
hp_precision <- numeric()
hp_classifier <- character()
hp_duration <- numeric()

hp_precision[1] <- mlp_highest_precision$precision
hp_classifier[1] <- "MultilayerPerceptron"
hp_duration[1] <- mlp_highest_precision$duration

hp_precision[2] <- rf_highest_precision$precision
hp_classifier[2] <- "RandomForest"
hp_duration[2] <- rf_highest_precision$duration

hp_precision[3] <- sl_highest_precision$precision
hp_classifier[3] <- "SimpleLogistic"
hp_duration[3] <- sl_highest_precision$duration

highest_precisions <- data.frame(precision=hp_precision, classifier=hp_classifier, duration=hp_duration, stringsAsFactors=FALSE) 
highest_precisions

ggplot(data=highest_precisions, aes(x=classifier, y=precision, fill=as.factor(classifier))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.96,0.9655)) +
  xlab("Classifier") +
  ylab("Precision") +
  ggtitle("Highest Precision per Classifier") +
  guides(fill=guide_legend("Classifiers"))

ggplot(data=highest_precisions, aes(x=classifier, y=duration/1000, fill=as.factor(classifier))) +
  geom_bar(position="dodge", stat="identity") +
  xlab("Classifier") +
  ylab("Duration") +
  ggtitle("Duration of Highest Precision per Classifier") +
  guides(fill=guide_legend("Classifiers"))

hr_recall <- numeric()
hr_classifier <- character()
hr_duration <- numeric()

hr_recall[1] <- mlp_highest_recall$recall
hr_classifier[1] <- "MultilayerPerceptron"
hr_duration[1] <- mlp_highest_recall$duration

hr_recall[2] <- rf_highest_recall$recall
hr_classifier[2] <- "RandomForest"
hr_duration[2] <- rf_highest_recall$duration

hr_recall[3] <- sl_highest_recall$recall
hr_classifier[3] <- "SimpleLogistic"
hr_duration[3] <- sl_highest_recall$duration

highest_recalls <- data.frame(recall=hr_recall, classifier=hr_classifier, duration=hr_duration, stringsAsFactors=FALSE) 
highest_recalls

ggplot(data=highest_recalls, aes(x=classifier, y=recall, fill=as.factor(classifier))) +
  geom_bar(position="dodge", stat="identity") +
  coord_cartesian(ylim=c(0.92,0.95)) +
  xlab("Classifier") +
  ylab("Recall") +
  ggtitle("Highest Recall per Classifier") +
  guides(fill=guide_legend("Classifiers"))

ggplot(data=highest_recalls, aes(x=classifier, y=duration/1000, fill=as.factor(classifier))) +
  geom_bar(position="dodge", stat="identity") +
  xlab("Classifier") +
  ylab("Duration") +
  ggtitle("Duration of Highest Recall per Classifier") +
  guides(fill=guide_legend("Classifiers"))
```

- Considering precious comments and results, we would recomend to use Random Forest, with the configuration used to get the best recall (max_depth = 25, num_features = 25 and num_trees = 150), because it offers a precision near the best one (96.44%) and the best Recall (94.39%).

- One other factor to take a look is the duration of execution. Way better than the one of the Multilayer Perceptron and very close of the Simple Logistic one.