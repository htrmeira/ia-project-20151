package br.edu.ufcg.dsc.ia.operators;

import java.io.File;
import java.io.IOException;

/**
 * @author Antonio Rodrigues - antonio.rodrigues@ccc.ufcg.edu.br
 *         Heitor Meira      - htrmeira@gmail.com
 *
 */
public interface Operator {
	/**
	 * Generates a representation as a double array of the given image file.
	 * 
	 * @param imageFile The image to represent as a double array.
	 * @return image representation.
	 * @throws IOException if an error occur during file reading.
	 */
	public double[] generateRepresentation(File imageFile) throws IOException;
}
