package br.edu.ufcg.dsc.ia.operators;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Extracts pixels from an image file and converts the RGB values to a 8-bit
 * gray scale representation, then constructs a histogram based on the percentage
 * counts of the gray scale.
 * 
 * @author Antonio Rodrigues - antonio.rodrigues@ccc.ufcg.edu.br
 *         Heitor Meira      - htrmeira@gmail.com
 *
 */
public class HistogramOperator implements Operator {
	/*
	 * Size of histogram is 256 for 8 bit representations.
	 */
	private static final int HIST_WIDTH = 256;
	/*
	 * 0 to 100 values of gray intensity.
	 */
	private static final int HIST_HEIGHT = 100;
	
	private static final double LUMINANCE_RED = 0.299D;
	private static final double LUMINANCE_GREEN = 0.587D;
	private static final double LUMINANCE_BLUE = 0.114D;

	@Override
	public double[] generateRepresentation(File imageFile) throws IOException {
		BufferedImage input = ImageIO.read(imageFile);
		int width = input.getWidth();
		int height = input.getHeight();
		List<Integer> graylevels = new ArrayList<Integer>();
		double maxWidth = 0.0D;
		double maxHeight = 0.0D;
		for (int row = 0; row < width; row++) {
			for (int col = 0; col < height; col++) {
				Color rgbColor = new Color(input.getRGB(row, col));
				int graylevel = extractGrayLevel(rgbColor);
				graylevels.add(graylevel);
				maxHeight++;
				if (graylevel > maxWidth) {
					maxWidth = graylevel;
				}
			}
		}
		double[] histogram = new double[HIST_WIDTH];
		for (Integer graylevel : (new HashSet<Integer>(graylevels))) {
			int idx = graylevel;
			histogram[idx] += Collections.frequency(graylevels, graylevel)
					* HIST_HEIGHT / maxHeight;
		}
		return histogram;
	}

	private int extractGrayLevel(Color rgbColor) {
		return (int) (LUMINANCE_RED * rgbColor.getRed() 
				+ LUMINANCE_GREEN * rgbColor.getGreen()
				+ LUMINANCE_BLUE * rgbColor.getBlue());
	}

}
