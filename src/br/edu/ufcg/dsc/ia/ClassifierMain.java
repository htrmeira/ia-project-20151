package br.edu.ufcg.dsc.ia;

import java.util.Locale;

import br.edu.ufcg.dsc.ia.stdout.Logger;
import br.edu.ufcg.dsc.ia.stdout.StdoutLogger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.trees.RandomForest;

/**
 * Main class to start the classification. This class extracts the right
 * parameters for each classification method (algorithm). For this project we
 * have choose:
 * - MultilayerPerceptron (neural network)
 * - SimpleLogistic
 * - RandomForest
 * 
 * @author Antonio Rodrigues - antonio.rodrigues@ccc.ufcg.edu.br
 *         Heitor Meira      - htrmeira@gmail.com
 *
 */
public class ClassifierMain {
	private final static String MULTILAYER_PERCEPTRON = "MultilayerPerceptron";
	private final static String SIMPLE_LOGISTIC = "SimpleLogistic";
	private final static String RANDOM_FOREST = "RandomForest";

	private static boolean isVerbose(String[] args) {
		return Boolean.parseBoolean(args[0]);
	}

	private static String getTrainingPath(String[] args) {
		return args[1];
	}

	private static String getTestPath(String[] args) {
		return args[2];
	}

	private static String getClassifierMethod(String[] args) {
		return args[3];
	}

	public static Classifier getClassifier(String[] args) {
		String classifier = getClassifierMethod(args);
		if (classifier.equals(MULTILAYER_PERCEPTRON)) {
			return getMultilayerPerceptron(args);
		} else if (classifier.equals(SIMPLE_LOGISTIC)) {
			return getSimpleLogistic(args);
		} else if (classifier.equals(RANDOM_FOREST)) {
			return getRandomForest(args);
		} else {
			System.out.println("ERROR!");
			return null;
		}
	}

	/**
	 * Configures and instantiates MultilayerPerceptron classifier.
	 * 
	 * @param args
	 * @return
	 */
	private static MultilayerPerceptron getMultilayerPerceptron(String[] args) {
		MultilayerPerceptron mlp = new MultilayerPerceptron();
		mlp.setTrainingTime(getTrainingTime(args));
		mlp.setLearningRate(getLearningRate(args));
		mlp.setHiddenLayers(getHiddenLayers(args));
		mlp.setDecay(getDecay(args));
		return mlp;
	}

	private static int getTrainingTime(String[] args) {
		return Integer.parseInt(args[4]);
	}

	private static double getLearningRate(String[] args) {
		return Double.parseDouble(args[5]);
	}

	private static String getHiddenLayers(String[] args) {
		return args[6];
	}

	private static boolean getDecay(String[] args) {
		return Boolean.parseBoolean(args[7]);
	}

	/**
	 * Configures and instantiates SimpleLogistic classifier.
	 * @param args
	 * @return
	 */
	private static SimpleLogistic getSimpleLogistic(String[] args) {
		SimpleLogistic sl = new SimpleLogistic();
		sl.setHeuristicStop(getHeuristicStop(args));
		sl.setMaxBoostingIterations(getMaxBoostingIterations(args));
		sl.setUseCrossValidation(getUseCrossValidation(args));
		return sl;
	}

	private static int getHeuristicStop(String[] args) {
		return Integer.parseInt(args[4]);
	}

	private static int getMaxBoostingIterations(String[] args) {
		return Integer.parseInt(args[5]);
	}

	private static boolean getUseCrossValidation(String[] args) {
		return Boolean.parseBoolean(args[6]);
	}

	/**
	 * Configures and instantiates RandomForest classifier.
	 * 
	 * @param args
	 * @return
	 */
	private static RandomForest getRandomForest(String[] args) {
		RandomForest rf = new RandomForest();
		rf.setMaxDepth(getMaxDepth(args));
		rf.setNumFeatures(getNumFeatures(args));
		rf.setNumTrees(getNumTrees(args));
		return rf;
	}

	private static int getMaxDepth(String[] args) {
		return Integer.parseInt(args[4]);
	}

	private static int getNumFeatures(String[] args) {
		return Integer.parseInt(args[5]);
	}
	
	private static int getNumTrees(String[] args) {
		return Integer.parseInt(args[6]);
	}

	public static void main(String[] args) throws Exception {
		boolean isVerbose = isVerbose(args);
		String trainingPath = getTrainingPath(args);
		String testingPath = getTestPath(args);

		Classifier classifierMethod = getClassifier(args);
		Logger logger = new StdoutLogger(isVerbose);
		ImagesClassifier classifier = new ImagesClassifier(trainingPath, testingPath, classifierMethod, isVerbose, logger);
		long initialTime = System.currentTimeMillis();
		Evaluation results = classifier.classify();
		long durationInMillis = System.currentTimeMillis() - initialTime;

		/**
		 * precision,recall,fmeasure,total_number_of_instances,correctly_classified,incorrectly_classified,kappa_statistic,
		 * mean_absolute_error,root_mean_squared_error,relative_absolute_error,root_relative_squared_error, duration
		 */
		logger.clean(String.format(Locale.getDefault(), results.weightedPrecision() + "," + results.weightedRecall() 
				+ "," + results.weightedFMeasure() + "," + results.numInstances() + "," + results.correct()
				+ "," + results.incorrect() + "," + results.kappa() + "," + results.meanAbsoluteError()
				+ "," + results.rootMeanSquaredError() + "," + results.relativeAbsoluteError()
				+ "," + results.rootRelativeSquaredError() + "," + durationInMillis));
	}
}
