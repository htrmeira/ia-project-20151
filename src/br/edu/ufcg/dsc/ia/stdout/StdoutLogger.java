package br.edu.ufcg.dsc.ia.stdout;

public class StdoutLogger implements Logger {
	private final boolean isVerbose;
	
	public StdoutLogger(boolean isVerbose) {
		this.isVerbose = isVerbose;
	}

	@Override
	public void debug(String content) {
		if (isVerbose) {
			System.out.println("debug: " + content);
		}
	}

	@Override
	public void info(String content) {
		System.out.println("info: " + content);
	}

	@Override
	public void info2(String content) {
		if (isVerbose) {
			System.out.println("info2: " + content);
		}
	}

	@Override
	public void warning(String content) {
		System.out.println("warning: " + content);
	}

	@Override
	public void error(String content) {
		System.err.println("error: " + content);
	}

	@Override
	public void clean(String content) {
		System.out.println(content);
	}

}
