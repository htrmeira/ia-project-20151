package br.edu.ufcg.dsc.ia.stdout;

public interface Logger {
	public void clean(String content);
	public void debug(String content);
	public void info(String content);
	public void info2(String content);
	public void warning(String content);
	public void error(String content);
}
