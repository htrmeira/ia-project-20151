package br.edu.ufcg.dsc.ia;

/**
 * DEPRECATED
 * @author heitor
 *
 */
public class ClassifierMain2 {
	public static void main(String[] args) throws Exception {
		double[] testClassifier = ImagesClassifier2.main(args);

		System.out.println("precision: " + testClassifier[0]);
		System.out.println("recall: " +  testClassifier[1]);
		System.out.println("f-measure: " +  testClassifier[2]);
	}
}
