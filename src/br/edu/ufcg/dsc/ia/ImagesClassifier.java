package br.edu.ufcg.dsc.ia;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import br.edu.ufcg.dsc.ia.operators.HistogramOperator;
import br.edu.ufcg.dsc.ia.operators.Operator;
import br.edu.ufcg.dsc.ia.stdout.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 * 
 * @author Antonio Rodrigues - antonio.rodrigues@ccc.ufcg.edu.br
 *         Heitor Meira      - htrmeira@gmail.com
 *
 */
public class ImagesClassifier {
	/*
	 * Portion of weka attributes related to gray scale values 
	 */
	private static final int GRAYSCALE_ELEMENTS_SIZE = 256;
	/*
	 * Total capacity of the vector (gray scale attributes + classes)
	 */
	private static final int CAPACITY = GRAYSCALE_ELEMENTS_SIZE + 1;

	/*
	 * We are considering two classes: picture with face or without (non-face).
	 * This forms one attribute with two classes.
	 */
	private static final String FACE = "face";
	private static final String NON_FACE = "non-face";
	private static final int CLASS_SIZE = 2;
	private static final int CLASSES_ATTIBUTE_SIZE = 1;

	private static final String TRAINING_RELATION_NAME = "TrainingRelation";
	private static final String TESTING_RELATION_NAME = "TestingRelation";
	
	private final Operator histogramOperator;
	private final String trainingPath;
	private final String testingPath;
	private final boolean isVerbose;
	private final Classifier classifierMethod;
	private final Logger logger;
	
	public ImagesClassifier(String trainingPath, String testingPath,
			Classifier classifierMethod, boolean isVerbose, Logger logger) {
		histogramOperator = new HistogramOperator();
		this.trainingPath = trainingPath;
		this.testingPath = testingPath;
		this.classifierMethod = classifierMethod;
		this.isVerbose = isVerbose;
		this.logger = logger;
	}

	public Evaluation classify() throws Exception {
		FastVector wekaAttributes = new FastVector(CAPACITY);

		addGrayScaleAttributes(wekaAttributes);
		addClassesAttributes(wekaAttributes);

		Instances trainingSet = createInstances(TRAINING_RELATION_NAME, wekaAttributes);

		Evaluation evaluationTest = createEvaluationTest(trainingSet);

		logger.debug("Executing training...");
		executeTraining(trainingPath, classifierMethod, trainingSet, wekaAttributes);
		logger.debug("Executing test...");
		executeTest(testingPath, classifierMethod, wekaAttributes, evaluationTest, trainingSet);
		logger.debug("Finished...");

		return evaluationTest;
	}

	/**
	 * Executes training.
	 * 
	 * @param trainPath
	 *            path to training set.
	 * @param classifierMethod
	 *            classifier method (algorithm) [MultilayerPerceptron,
	 *            NaiveBayes, RandomForest etc]
	 * @param trainingSet
	 * @param wekaAttributes weka configuration attributes.
	 * @throws Exception
	 *             If some error occurs while reading image files or during
	 *             classification.
	 */
	public void executeTraining(String trainPath, Classifier classifierMethod,
			Instances trainingSet, FastVector wekaAttributes) throws Exception {

		String faceFolder = trainPath + File.separator + FACE;
		String nonFaceFolder = trainPath + File.separator + NON_FACE;

		logger.debug("Training: " + FACE);
		buildTrainingSet(wekaAttributes, trainingSet, faceFolder, FACE);
		logger.debug("Training: " + NON_FACE);
		buildTrainingSet(wekaAttributes, trainingSet, nonFaceFolder, NON_FACE);

		classifierMethod.buildClassifier(trainingSet);
	}

	/**
	 * Executes the tests.
	 * 
	 * @param testingPath
	 * @param classifierMethod
	 * @param wekaAttributes
	 * @param evaluationTest
	 * @throws Exception
	 */
	public void executeTest(String testingPath, Classifier classifierMethod,
			FastVector wekaAttributes, Evaluation evaluationTest, Instances trainingSet) throws Exception {

		Instances testingSet = createInstances(TESTING_RELATION_NAME, wekaAttributes);

		String faceFolder = testingPath + File.separator + FACE;
		String nonFaceFolder = testingPath + File.separator + NON_FACE;

		logger.debug("Testing: " + FACE);
		buildTrainingSet(wekaAttributes, testingSet, faceFolder, FACE);
		logger.debug("Testing: " + NON_FACE);
		buildTrainingSet(wekaAttributes, testingSet, nonFaceFolder, NON_FACE);

		evaluationTest.evaluateModel(classifierMethod, testingSet);

		debugEvaluation(classifierMethod, testingSet, faceFolder, nonFaceFolder, trainingSet);
	}

	private void debugEvaluation(Classifier cModel, 
			Instances testingSet, String faceFolder,
			String nonFaceFolder, Instances trainingSet) throws Exception {
		if (isVerbose) {
			logger.info2(String.format(Locale.getDefault(), "%s,%s,%s", "real_class", "estimated_class", "file_name"));
			File[] files1 = new File(faceFolder).listFiles();
			File[] files2 = new File(nonFaceFolder).listFiles();
			
			debugEstimatives(FACE, files1, testingSet, trainingSet, cModel, 0);
			debugEstimatives(NON_FACE, files2, testingSet, trainingSet, cModel, files1.length);
		}
	}

	private void debugEstimatives(String originalClass, File[] files,
			Instances testingSet, Instances trainingSet, Classifier cModel,
			int start) throws Exception {
		for (int i = 0; i < files.length; i++) {
			Instance copiedInstance = (Instance) testingSet.instance(start + i).copy();
			String estimatedClass = trainingSet.classAttribute().value((int) cModel.classifyInstance(copiedInstance));

			logger.info2(String.format(Locale.getDefault(), "%s,%s,%s",
					originalClass, estimatedClass, files[i].getAbsolutePath()));
		}
	}

	/**
	 * 
	 * @param wekaAttributes
	 * @param trainingSet
	 * @param folderName
	 * @param classe
	 * @throws IOException
	 */
	private void buildTrainingSet(FastVector wekaAttributes,
			Instances trainingSet, String folderName, String classe) throws IOException {
		File folder = new File(folderName);
		File[] listOfFiles = folder.listFiles();

		for (File f : listOfFiles) {
			double[] histogram = histogramOperator.generateRepresentation(f);
			createTrainingSet(trainingSet, wekaAttributes, histogram, classe);
		}
	}

	/**
	 * Configures an image instance with histogram profile and elected class, and add to training set.
	 * 
	 * @param trainingSet
	 * @param wekaAttributes
	 * @param histogram
	 * @param classe
	 */
	private void createTrainingSet(Instances trainingSet, FastVector wekaAttributes, double[] histogram, String classe) {
		Instance imageInstance = new Instance(CAPACITY);
		for (int i = 0; i < histogram.length; i++) {
			imageInstance.setValue((Attribute) wekaAttributes.elementAt(i), histogram[i]);
		}
		if (!classe.isEmpty()) {
			imageInstance.setValue((Attribute) wekaAttributes.elementAt(GRAYSCALE_ELEMENTS_SIZE), classe);
		}
		trainingSet.add(imageInstance);
	}

	/**
	 * 
	 * @param trainingSet
	 * @return
	 * @throws Exception
	 */
	private Evaluation createEvaluationTest(Instances trainingSet) throws Exception {
		return new Evaluation(trainingSet);
	}

	private Instances createInstances(String relationName, FastVector wekaAttributes) {
		Instances trainingSet = new Instances(relationName, wekaAttributes, CLASSES_ATTIBUTE_SIZE);
		trainingSet.setClassIndex(GRAYSCALE_ELEMENTS_SIZE);
		return trainingSet;
	}

	/**
	 * Adds the generated classes to weka attributes.
	 * 
	 * @param wekaAttributes
	 */
	private void addClassesAttributes(FastVector wekaAttributes) {
		Attribute classAttribute = generateClassAttributes();
		wekaAttributes.addElement(classAttribute);
	}

	/**
	 * Creating two classes of images, with faces (face) and without (non-face).
	 * 
	 * @return These classes as and attribute.
	 */
	private Attribute generateClassAttributes() {
		FastVector classes = new FastVector(CLASS_SIZE);
		classes.addElement(FACE);
		classes.addElement(NON_FACE);
		return new Attribute("classes", classes);
	}

	/**
	 * Generates and adds gray scale attributes to weka attributes. 
	 * 
	 * @param wekaAttributes
	 */
	private void addGrayScaleAttributes(FastVector wekaAttributes) {
		for (int i = 0; i < GRAYSCALE_ELEMENTS_SIZE; i++) {
			Attribute attr = new Attribute("numeric" + i);
			wekaAttributes.addElement(attr);
		}
	}

}
